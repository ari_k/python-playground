import re
import urllib2

with open('input.txt', 'r') as infile:
    data = infile.read()

idlist = []
for line in data.split('/n'):
    line = line.strip()
    url = "https://www.youtube.com/results?search_query={}".format(line.replace(' ', '+'))
    pagedata = urllib2.urlopen(url, timeout=10)
    results = pagedata.read()
    found = re.search('"/watch\?v=(.*?)"', results)
    if(found):
        idlist.append(found.group(1))

with open('output.txt', 'w') as outfile:
    for videoid in idlist:
        outfile.write("https://www.youtube.com/watch?v={}\n".format(videoid))
